<?php
/*
Plugin Name: BuilderPress
Plugin URI: http://thimpress.com/
Description: Full of Thim features for page builders: Visual Composer, Site Origin, Elementor
Author: ThimPress
Version: 1.0.0
Text Domain: builderpress
Author URI: http://thimpress.com
*/

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress' ) ) {
	/**
	 * Class BuilderPress
	 */
	final class BuilderPress {

		/**
		 * @var null
		 */
		private static $_instance = null;

		/**
		 * @var string
		 */
		public $_version = '1.0.0';

		/**
		 * BuilderPress constructor.
		 */
		public function __construct() {

			if ( ! function_exists( 'is_plugin_active' ) ) {
				include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			}

			// Depend on Visual Composer
			if ( ! ( is_plugin_active( 'js_composer/js_composer.php' ) || is_plugin_active( 'elementor/elementor.php' ) || is_plugin_active( 'siteorigin-panels/siteorigin-panels.php' ) ) ) {
				add_action( 'admin_notices', array( $this, 'admin_notices' ) );
			} else {
				$this->define_constants();
				$this->includes();
				$this->init_hooks();
			}
		}

		/**
		 * Define constants.
		 */
		private function define_constants() {
			define( 'BUILDER_PRESS_FILE', __FILE__ );
			define( 'BUILDER_PRESS_PATH', dirname( __FILE__ ) . '/' );
			define( 'BUILDER_PRESS_URL', plugins_url( '', __FILE__ ) . '/' );
			define( 'BUILDER_PRESS_INC', BUILDER_PRESS_PATH . 'inc/' );
			define( 'BUILDER_PRESS_VER', $this->_version );
			define( 'BUILDER_PRESS_PREMIUM', 'https://thimpress.com/' );
		}

		/**
		 * Include files.
		 */
		private function includes() {
			require_once( BUILDER_PRESS_INC . 'class-bp-autoloader.php' );

			require_once( BUILDER_PRESS_INC . 'abstracts/class-bp-abstract-shortcode.php' );
			require_once( BUILDER_PRESS_INC . 'abstracts/class-bp-abstract-widget.php' );

			require_once( BUILDER_PRESS_INC . 'builders/visual-composer/class-bp-vc-shortcode.php' );
			require_once( BUILDER_PRESS_INC . 'builders/siteorigin/class-bp-so-widget.php' );

			require_once( BUILDER_PRESS_INC . 'builders/visual-composer/class-bp-vc.php' );
			require_once( BUILDER_PRESS_INC . 'builders/siteorigin/class-bp-so.php' );
			require_once( BUILDER_PRESS_INC . 'builders/elementor/class-bp-el.php' );

			require_once( BUILDER_PRESS_INC . 'features/general/brands/dataset.php' );

			require_once( BUILDER_PRESS_INC . 'functions.php' );
		}

		/**
		 * Init hook.
		 */
		public function init_hooks() {

			// admin scripts
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );

			// define ajaxurl if not exist
			add_action( 'wp_head', array( $this, 'define_ajaxurl' ), 1000 );

			// plugin links in admin list plugins
			add_filter( "plugin_action_links_builderpress/builderpress.php", array( __CLASS__, 'plugin_links' ) );
		}

		/**
		 * Register admin scripts
		 */
		public function admin_scripts() {
			// datetimepicker
			wp_register_style( 'builderpress-datetimepicker', BUILDER_PRESS_URL . 'assets/lib/datetimepicker/jquery.datetimepicker.min.css' );
			wp_register_script( 'builderpress-datetimepicker', BUILDER_PRESS_URL . 'assets/lib/datetimepicker/jquery.datetimepicker.full.min.js', array( 'jquery', ), '', true );

			wp_enqueue_style( 'builderpress-admin', BUILDER_PRESS_URL . 'assets/css/admin-builderpress.css', array(), BUILDER_PRESS_VER );
		}

		/**
		 * Define ajaxurl if not exist
		 */
		public function define_ajaxurl() { ?>
            <script type="text/javascript">
                if (typeof ajaxurl === 'undefined') {
                    /* <![CDATA[ */
                    var ajaxurl = "<?php echo esc_js( admin_url( 'admin-ajax.php' ) ); ?>";
                    /* ]]> */
                }
            </script>
			<?php
		}

		/**
		 * @param $links
		 *
		 * @return array
		 */
		public static function plugin_links( $links ) {
			$links[] = '<a href="' . BUILDER_PRESS_PREMIUM . '" target="_blank">' . __( 'Upgrade to premium', 'builderpress' ) . '</a>';

			return $links;
		}

		/**
		 * Admin notice
		 */
		public function admin_notices() {
			?>
            <div class="notice notice-error">
                <p>
					<?php echo wp_kses(
						__( '<strong>BuilderPress</strong> plugin supports for <strong>Visual Composer, Siteorigin or Elementor</strong>. Please install and activate one of the page builders.', 'builderpress' ),
						array(
							'strong' => array()
						)
					); ?>
                </p>
            </div>
		<?php }

		/**
		 * @return null|BuilderPress
		 */
		public static function instance() {
			if ( ! self::$_instance ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}
	}
}

if ( ! function_exists( 'BuilderPress' ) ) {
	/**
	 * @return null|BuilderPress
	 */
	function BuilderPress() {
		return BuilderPress::instance();
	}
}

$GLOBALS['BuilderPress'] = BuilderPress();