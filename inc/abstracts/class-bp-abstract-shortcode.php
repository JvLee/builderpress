<?php
/**
 * BuilderPress Abstract shortcode
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes/Abstract
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_Abstract_Shortcode' ) ) {

	/**
	 * Class BuilderPress_Abstract_Shortcode
	 */
	abstract class BuilderPress_Abstract_Shortcode {

		public function __construct() {
		}
	}
}