<?php
/**
 * BuilderPress Abstract widget
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes/Abstract
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_Abstract_Widget' ) ) {

	/**
	 * Class BuilderPress_Abstract_Widget
	 */
	abstract class BuilderPress_Abstract_Widget {

		public function __construct() {
		}
	}
}