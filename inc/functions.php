<?php
/**
 * BuilderPress functions
 *
 * @version     1.0.0
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'builderpress_so_params_converter' ) ) {
	/**
	 * @param $params
	 *
	 * @return mixed
	 */
	function builderpress_so_params_converter( $params ) {
		if ( ! is_array( $params ) ) {
			return $params;
		}

		return $params;
	}
}

if ( ! function_exists( 'builderpress_el_params_converter' ) ) {
	/**
	 * @param $params
	 *
	 * @return mixed
	 */
	function builderpress_el_params_converter( $params ) {
		if ( ! is_array( $params ) ) {
			return $params;
		}

		return $params;
	}
}

if ( ! function_exists( 'builder_press_get_template' ) ) {
	/**
	 * @param        $template_name
	 * @param array  $args
	 * @param string $tempate_path
	 * @param string $default_path
	 */
	function builder_press_get_template( $template_name, $args = array(), $tempate_path = '', $default_path = '' ) {
		if ( is_array( $args ) && isset( $args ) ) {
			extract( $args );
		}

		$template_name = $template_name . '.php';

		$template_file = builder_press_locate_template( $template_name, $tempate_path, $default_path );

		if ( ! file_exists( $template_file ) ) {
			_doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $template_file ), '1.0.0' );

			return;
		}

		include $template_file;
	}
}

if ( ! function_exists( 'builder_press_locate_template' ) ) {
	/**
	 * @param        $template_name
	 * @param string $template_path
	 * @param string $default_path
	 *
	 * @return mixed
	 */
	function builder_press_locate_template( $template_name, $template_path = '', $default_path = '' ) {

		if ( ! $template_path ) {
			$template_path = 'features/';
		}

		// Set default plugin templates path.
		if ( ! $default_path ) {
			$default_path = BUILDER_PRESS_INC . 'features/' . $template_path; // Path to the template folder
		}

		// Search template file in theme folder.
		$template = locate_template( array(
			'builderpress/' . $template_path . $template_name,
			$template_name
		) );

		// Get plugins template file.
		if ( ! $template ) {
			$template = $default_path . $template_name;
		}

		return apply_filters( 'builder-press/locate-template', $template, $template_name, $template_path, $default_path );
	}
}