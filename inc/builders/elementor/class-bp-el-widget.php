<?php
/**
 * BuilderPress
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
//defined( 'ABSPATH' ) || exit;

namespace Builderpress;

use \Elementor\Widget_Base;

if ( ! class_exists( 'BuilderPress_El_Widget' ) ) {
	/**
	 * Class BuilderPress_El_Widget
	 */
	abstract class BuilderPress_El_Widget extends Widget_Base {

		/**
		 * @return string
		 */
		public function get_icon() {
			return 'eicon-plus-square';
		}

		/**
		 * @return array
		 */
		public function get_categories() {
			return array( 'builder-press' );
		}

		/**
		 * Render template
		 */
		protected function _content_template() {
		}
	}

}