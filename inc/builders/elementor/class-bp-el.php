<?php
/**
 * BuilderPress handler class
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_El' ) ) {
	/**
	 * Class BuilderPress_El
	 */
	class BuilderPress_El {

		/**
		 * @var null
		 */
		private static $instance = null;

		/**
		 * BuilderPress_El constructor.
		 */
		public function __construct() {
			if ( ! is_plugin_active( 'elementor/elementor.php' ) ) {
				return;
			}

			// add widget categories
			add_action( 'elementor/init', array( $this, 'register_categories' ) );
			// load widget
			add_action( 'elementor/widgets/widgets_registered', array( $this, 'load_widgets' ) );
		}

		/**
		 * Add widget categories
		 */
		public function register_categories() {
			\Elementor\Plugin::instance()->elements_manager->add_category(
				'builder-press',
				array(
					'title' => __( 'BuilderPress', 'builderpress' ),
					'icon'  => 'fa fa-plug'
				)
			);
		}

		/**
		 * @param $widgets_manager Elementor\Widgets_Manager
		 *
		 * @throws Exception
		 */
		public function load_widgets( $widgets_manager ) {

			require_once( BUILDER_PRESS_INC . 'builders/elementor/class-bp-el-widget.php' );

			require_once( BUILDER_PRESS_INC . 'features/general/brands/class-el-widget-brands.php' );

			$widgets_manager->register_widget_type( new \Builderpress\BuilderPress_El_Widget_Brands() );
		}

		/**
		 * Instance.
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
		}
	}
}

new BuilderPress_El();