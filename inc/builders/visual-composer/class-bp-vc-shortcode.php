<?php
/**
 * BuilderPress VC Shortcode
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_VC_Shortcode' ) ) {
	/**
	 * Class BuilderPress_VC_Shortcode
	 */
	class BuilderPress_VC_Shortcode extends BuilderPress_Abstract_Shortcode {
		/**
		 * @var string
		 */
		protected $base = '';

		/**
		 * @var string
		 */
		protected $name = '';

		/**
		 * @var string
		 */
		protected $desc = '';

		/**
		 * @var string
		 */
		protected $group = 'general';

		/**
		 * @var string
		 */
		protected $assets_url = '';

		/**
		 * @var string
		 */
		protected $assets_path = '';

		/**
		 * BuilderPress_Abstract_Shortcode constructor.
		 */
		public function __construct() {

			$this->assets_url  = BUILDER_PRESS_URL . 'inc/features/' . $this->group . '/' . $this->base . '/assets/';
			$this->assets_path = BUILDER_PRESS_INC . 'features/' . $this->group . '/' . $this->base . '/assets/';

			$this->settings();

			add_shortcode( 'thim-' . $this->base, array( $this, 'shortcode' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}

		/**
		 * Shortcode setting fields
		 */
		public function settings() {

			// shortcode params
			$params = apply_filters( 'builder-press/shorcode-params/' . $this->base, $this->params() );

			foreach ( $params as $key => $param ) {
				if ( isset( $param['paid'] ) && $param['paid'] ) {
					// param class
					$param_holder_class = isset( $param['param_holder_class'] ) ? $param['param_holder_class'] : '';

					// update heading to suggest premium version
					$params[ $key ]['heading'] = $param['heading'] . __( ' (Premium feature) ', 'builderpress' ) . '<a href="' . BUILDER_PRESS_PREMIUM . '" target="_blank">' . __( 'Upgrade to premium version to unlock all feature', 'builderpress' ) . '</a>';
					// add custom class
					$params[ $key ]['param_holder_class'] = $param_holder_class . 'bp-premium-param';
				}
			}

			// get global icon for shortcode not defined icon
			$icon = file_exists( $this->assets_path . 'images/icon.png' ) ? $this->assets_url . 'images/icon.png' : BUILDER_PRESS_URL . 'assets/images/icon.png';

			vc_map(
				array(
					'name'        => $this->name,
					'base'        => 'thim-' . $this->base,
					'category'    => __( 'BuilderPress', 'builderpress' ),
					'description' => $this->desc,
					'icon'        => $icon,
					'params'      => is_array( $params ) ? $params : array()
				)
			);
		}

		/**
		 * Shortcode params.
		 *
		 * @return array
		 */
		public function params() {
			return array();
		}

		/**
		 * Output
		 */
		/**
		 * @param $atts
		 */
		public function shortcode( $atts ) {
			return;
		}

		/**
		 * Enqueue scripts.
		 */
		public function enqueue_scripts() {
			return;
		}

		/**
		 * @param $params
		 *
		 * //         * @return string
		 */
		public function output( $params ) {

			$params = apply_filters( 'builder-press/shortcode-atts/' . $this->base, $params );

//			ob_start();
			builder_press_get_template( 'default', array( 'params' => $params ), $this->group . '/' . $this->base . '/tpl/' );
//			$html = ob_get_clean();

//			return $html;
		}
	}

}