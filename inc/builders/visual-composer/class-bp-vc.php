<?php
/**
 * BuilderPress handler class
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_VC' ) ) {
	/**
	 * Class BuilderPress_VC
	 */
	class BuilderPress_VC {
		/**
		 * BuilderPress_VC constructor.
		 */
		public function __construct() {

			if ( ! is_plugin_active( 'js_composer/js_composer.php' ) ) {
				return;
			}

			// register extra params
			add_action( 'vc_before_init', array( $this, 'register_extra_params' ) );

			add_action( 'vc_before_init', array( $this, 'load_shortcodes' ) );
		}

		/**
		 * Load shortcodes
		 */
		public function load_shortcodes() {
			require_once( BUILDER_PRESS_INC . 'features/general/brands/class-vc-shortcode-brands.php' );
		}

		/**
		 * Register VC extra params.
		 */
		public function register_extra_params() {
			if ( function_exists( 'vc_add_shortcode_param' ) ) {
				vc_add_shortcode_param( 'number', array( $this, '_number_param' ) );
				vc_add_shortcode_param( 'radio_image', array( $this, '_radio_image_param' ) );
				vc_add_shortcode_param( 'datepicker', array( $this, '_datepicker_param' ) );

				do_action( 'builder-press/register-extra-params' );
			}
		}

		/**
		 * @param $settings
		 * @param $value
		 *
		 * @return string
		 */
		public function _number_param( $settings, $value ) {
			$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
			$type       = isset( $settings['type'] ) ? $settings['type'] : '';
			$min        = isset( $settings['min'] ) ? $settings['min'] : '';
			$max        = isset( $settings['max'] ) ? $settings['max'] : '';
			$suffix     = isset( $settings['suffix'] ) ? $settings['suffix'] : '';
			$class      = isset( $settings['class'] ) ? $settings['class'] : '';
			$value      = isset( $value ) ? $value : $settings['value'];
			$output     = '<input type="number" min="' . $min . '" max="' . $max . '" class="wpb_vc_param_value ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . $value . '" style="max-width:100px; margin-right: 10px;" />' . $suffix;

			return $output;
		}

		/**
		 * @param $settings
		 * @param $value
		 *
		 * @return string
		 */
		public function _radio_image_param( $settings, $value ) {
			$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
			$type       = isset( $settings['type'] ) ? $settings['type'] : '';
			$radios     = isset( $settings['options'] ) ? $settings['options'] : '';
			$class      = isset( $settings['class'] ) ? $settings['class'] : '';
			$output     = '<input type="hidden" name="' . $param_name . '" id="' . $param_name . '" class="wpb_vc_param_value ' . $param_name . ' ' . $type . '_field ' . $class . '" value="' . $value . '"  ' . ' />';
			$output     .= '<div id="' . $param_name . '_wrap" class="icon_style_wrap ' . $class . '" >';
			if ( $radios != '' && is_array( $radios ) ) {
				$i = 0;
				foreach ( $radios as $key => $image_url ) {
					$class   = ( $key == $value ) ? ' class="selected" ' : '';
					$image   = '<img id="' . $param_name . $i . '_img' . $key . '" src="' . $image_url . '" ' . $class . '/>';
					$checked = ( $key == $value ) ? ' checked="checked" ' : '';
					$output  .= '<input name="' . $param_name . '_option" id="' . $param_name . $i . '" value="' . $key . '" type="radio" '
					            . 'onchange="document.getElementById(\'' . $param_name . '\').value=this.value;'
					            . 'jQuery(\'#' . $param_name . '_wrap img\').removeClass(\'selected\');'
					            . 'jQuery(\'#' . $param_name . $i . '_img' . $key . '\').addClass(\'selected\');'
					            . 'jQuery(\'#' . $param_name . '\').trigger(\'change\');" '
					            . $checked . ' style="display:none;" />';
					$output  .= '<label for="' . $param_name . $i . '">' . $image . '</label>';
					$i ++;
				}
			}
			$output .= '</div>';

			return $output;
		}

		/**
		 * @param $settings
		 * @param $value
		 *
		 * @return string
		 */
		public function _datepicker_param( $settings, $value ) {
			$param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
			$type       = isset( $settings['type'] ) ? $settings['type'] : '';
			$class      = isset( $settings['class'] ) ? $settings['class'] : '';
			$value      = isset( $value ) ? $value : $settings['value'];
			$output     = '<input type="text" name="' . $param_name . '" class="thim-datetimepicker wpb_vc_param_value ' . $param_name . ' ' . $type . '_field ' . $class . '" value="' . $value . '"  />';
			$output     .= '<script>jQuery(\'.thim-datetimepicker\').datetimepicker();</script>';
			$output     .= '';

			return $output;
		}
	}
}

new BuilderPress_VC();