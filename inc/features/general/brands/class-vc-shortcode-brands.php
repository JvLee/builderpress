<?php
/**
 * BuilderPress Visual Composer Brands shortcode
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_VC_Shortcode_Brands' ) ) {
	/**
	 * Class BuilderPress_VC_Shortcode_Brands
	 */
	class BuilderPress_VC_Shortcode_Brands extends BuilderPress_VC_Shortcode {

		/**
		 * BuilderPress_VC_Shortcode_Brands constructor.
		 */
		public function __construct() {
			// config
			$this->base = BuilderPress_Brands_Dataset::$base;
			$this->name = BuilderPress_Brands_Dataset::$name;
			$this->desc = BuilderPress_Brands_Dataset::$desc;

			parent::__construct();
		}

		/**
		 * @return array
		 */
		public function params() {
			return BuilderPress_Brands_Dataset::$options;
		}

		/**
		 * Enqueue scripts
		 */
		public function enqueue_scripts() {
			if ( ! wp_script_is( 'builder-press-owl-carousel' ) ) {
				wp_enqueue_style( 'builder-press-owl-carousel', $this->assets_url . 'css/owl.carousel.min.css', array(), '2.3.4' );
				wp_enqueue_script( 'builder-press-owl-carousel', $this->assets_url . 'js/owl.carousel.min.js', array( 'jquery' ), '2.3.4', true );
			}

			wp_enqueue_style( 'builder-press-sc-brands', $this->assets_url . 'css/brands.min.css', array(), BUILDER_PRESS_VER );
			wp_enqueue_script( 'builder-press-sc-brands', $this->assets_url . 'js/brands.js', array(
				'jquery',
				'builder-press-owl-carousel'
			), BUILDER_PRESS_VER, true );
		}

		/**
		 *
		 * @param $atts
		 */
		public function shortcode( $atts ) {
			$params = shortcode_atts( array(
				'items'         => '',
				'items_visible' => '6',
				'items_tablet'  => '4',
				'items_mobile'  => '2',
				'nav'           => 'no',
				'el_class'      => '',
			), $atts );

			$params['items'] = vc_param_group_parse_atts( $params['items'] );

			parent::output( $params );
		}
	}
}

new BuilderPress_VC_Shortcode_Brands();