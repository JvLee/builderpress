(function ($) {
	"use strict";

	var thim_sc_brands = window.thim_sc_brands = {

		init: function () {
			var $thim_brands = $('.thim-brands');

			$thim_brands.each(function () {
				var items_visible = $(this).attr('data-items-visible'),
					items_tablet = $(this).attr('data-items-tablet'),
					items_mobile = $(this).attr('data-items-mobile'),
					rtl = $('body').hasClass('rtl');

				$(this).find('.owl-carousel').owlCarousel({
						rtl: rtl,
						responsive: {
							0   : {
								items: items_mobile,
							},
							600   : {
								items: 2,
							},
							768 : {
								items: items_tablet,
							},
							1200: {
								items: items_visible,
							}
						},
					}
				)
			})
		}
	};

    $(document).ready(function () {
        thim_sc_brands.init();
    });

})(jQuery);