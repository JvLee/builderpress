<?php
/**
 * BuilderPress Siteorigin Brands shortcode
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_SO_Widget_Brands' ) ) {
	/**
	 * Class BuilderPress_SO_Widget_Brands
	 */
	class BuilderPress_SO_Widget_Brands extends BuilderPress_SO_Widget {

		/**
		 * BuilderPress_SO_Widget_Brands constructor.
		 */
		public function __construct() {

			$id             = BuilderPress_Brands_Dataset::$base;
			$name           = BuilderPress_Brands_Dataset::$name;
			$widget_options = array(
				'description'   => BuilderPress_Brands_Dataset::$desc,
				'help'          => '',
				'panels_groups' => array( 'builderpress_widgets' )
			);

//			$form_options = BuilderPress_Brands_Dataset::$options;
			$form_options = array();
			$base_folder  = BUILDER_PRESS_INC . 'features/general/brands';

			parent::__construct( $id, $name, $widget_options, array(), $form_options, $base_folder );
		}

		/**
		 * @param $instance
		 *
		 * @return mixed|string
		 */
		function get_template_name( $instance ) {
			return 'base';
		}

		/**
		 * @param $instance
		 *
		 * @return bool|mixed
		 */
		function get_style_name( $instance ) {
			return false;
		}
	}
}

if ( ! function_exists( 'builder_press_brands_so_widget' ) ) {
	function builder_press_brands_so_widget() {
		register_widget( 'BuilderPress_SO_Widget_Brands' );
	}
}

add_action( 'widgets_init', 'builder_press_brands_so_widget' );
