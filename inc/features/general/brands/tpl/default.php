<?php
/**
 * Template for displaying default template brands shortcode.
 *
 * This template can be overridden by copying it to yourtheme/builderpress/brands/default.php.
 *
 * @author      ThimPress
 * @package     BuilderPress/Templates
 * @version     1.0.0
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

/**
 * @var $params array - shortcode params
 */
?>

<div class="thim-brands <?php echo esc_attr( $params["el_class"] ); ?>"
     data-items-visible="<?php echo esc_attr( $params['items_visible'] ); ?>"
     data-items-tablet="<?php echo esc_attr( $params['items_tablet'] ); ?>"
     data-items-mobile="<?php echo esc_attr( $params['items_mobile'] ); ?>">

    <div class="container">
        <div class="owl-carousel owl-theme">
			<?php if ( is_array( $params['items'] ) && $params['items'] ) {
				foreach ( $params['items'] as $key => $brand ) {
					?>
                    <div class="item-brands">
						<?php if ( isset( $brand['img'] ) ) {
							$logo = wp_get_attachment_image_src( $brand['img'], 'full' );
							$img  = '<img src="' . $logo[0] . '" width="' . $logo[1] . '" height="' . $logo[2] . '" alt="' . esc_attr__( 'Logo', 'course-builder' ) . '">';

							if ( isset( $brand['link'] ) ) {
								$link = vc_build_link( $brand['link'] ); ?>
                                <a href="<?php echo esc_url( $link['url'] ) ?>"
                                   target="<?php echo esc_attr( $link['target'] ) ?>"><?php echo ent2ncr( $img ); ?></a>
							<?php } else {
								echo ent2ncr( $img );
							}
						} ?>
                    </div>

				<?php }
			} ?>
        </div>
    </div>

</div>