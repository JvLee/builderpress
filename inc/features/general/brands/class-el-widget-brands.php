<?php
/**
 * BuilderPress Siteorigin Brands shortcode
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

///**
// * Prevent loading this file directly
// */
//defined( 'ABSPATH' ) || exit;

namespace Builderpress;

use \Builderpress\BuilderPress_El_Widget;
use BuilderPress_Brands_Dataset;

if ( ! class_exists( 'BuilderPress_El_Widget_Brands' ) ) {
	/**
	 * Class BuilderPress_El_Widget_Brands
	 * @package Builderpress
	 */
	class BuilderPress_El_Widget_Brands extends BuilderPress_El_Widget {

		/**
		 * @return string
		 */
		public function get_name() {
			return BuilderPress_Brands_Dataset::$base;
		}

		/**
		 * @return string
		 */
		public function get_title() {
			return BuilderPress_Brands_Dataset::$name;
		}

		/**
		 * @return string
		 */
		public function get_icon() {
			return 'eicon-folder';
		}
	}
}