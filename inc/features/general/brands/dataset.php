<?php
/**
 * BuilderPress
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_Brands_Dataset' ) ) {
	/**
	 * Class BuilderPress_Brands_Dataset
	 */
	class BuilderPress_Brands_Dataset {

		/**
		 * @var string
		 */
		public static $group = '';

		/**
		 * @var string
		 */
		public static $base = '';

		/**
		 * @var string
		 */
		public static $name = '';

		/**
		 * @var string
		 */
		public static $desc = '';

		/**
		 * @var array
		 */
		public static $options = array();

		/**
		 * BuilderPress_Brands_Dataset constructor.
		 */
		public function __construct() {

			// info
			self::$base  = 'brands';
			self::$name  = __( 'Brands', 'builderpress' );
			self::$desc  = __( 'Display brands slider', 'builderpress' );
			self::$group = 'general';

			// options
			self::$options = array(
				array(
					'type'       => 'param_group',
					'heading'    => __( 'Brands', 'builderpress' ),
					'param_name' => 'items',
					'value'      => '',
					'params'     => array(
						array(
							'type'       => 'attach_image',
							'heading'    => esc_html__( 'Brand Image', 'builderpress' ),
							'param_name' => 'img'
						),
						array(
							'type'       => 'vc_link',
							'heading'    => esc_html__( 'Brand Link', 'builderpress' ),
							'param_name' => 'link'
						),
					),
				),

				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Visible items', 'builderpress' ),
					'param_name'       => 'items_visible',
					'std'              => '6',
					'admin_label'      => true,
					'edit_field_class' => 'vc_col-xs-4',
				),

				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Tablet Items', 'builderpress' ),
					'param_name'       => 'items_tablet',
					'std'              => '4',
					'admin_label'      => true,
					'edit_field_class' => 'vc_col-xs-4',
				),

				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Mobile Items', 'builderpress' ),
					'param_name'       => 'items_mobile',
					'std'              => '2',
					'admin_label'      => true,
					'edit_field_class' => 'vc_col-xs-4',
				),

				// Extra class
				array(
					'type'        => 'textfield',
					'admin_label' => true,
					'heading'     => esc_html__( 'Extra class', 'builderpress' ),
					'param_name'  => 'el_class',
					'value'       => '',
					'description' => esc_html__( 'Add extra class name for Thim Brands shortcode to use in CSS customizations.', 'builderpress' ),
				)
			);
		}
	}
}

new BuilderPress_Brands_Dataset();