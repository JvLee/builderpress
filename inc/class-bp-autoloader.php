<?php
/**
 * BuilderPress autoloader class
 *
 * @version     1.0.0
 * @author      ThimPress
 * @package     BuilderPress/Classes
 * @category    Classes
 * @author      Thimpress, leehld
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'BuilderPress_Autoloader' ) ) {
	/**
	 * Class BuilderPress_Autoloader
	 */
	class BuilderPress_Autoloader {

		/**
		 * BuilderPress_Autoloader constructor.
		 */
		public function __construct() {
			if ( function_exists( "__autoload" ) ) {
				spl_autoload_register( "__autoload" );
			}

			spl_autoload_register( array( $this, 'autoload' ) );
		}

		/**
		 * @param $class
		 */
		public function autoload( $class ) {

			$class = strtolower( $class );
			$path  = '';
			$file  = $this->get_file_name_from_class( $class );

			if ( stripos( $class, 'builderpress_abstract_' ) === 0 ) {
				$path = BUILDER_PRESS_INC . 'abstracts/';
			}

			$this->load_file( $path . $file );
		}

		/**
		 * @param $class
		 *
		 * @return string
		 */
		private function get_file_name_from_class( $class ) {
			return 'class-' . str_replace( '_', '-', strtolower( $class ) ) . '.php';
		}

		/**
		 * @param $path
		 *
		 * @return bool
		 */
		private function load_file( $path ) {
			if ( $path && is_readable( $path ) ) {
				include_once $path;

				return true;
			}

			return false;
		}
	}
}

new BuilderPress_Autoloader();